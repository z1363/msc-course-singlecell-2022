# MSc Course SingleCell 2022

## General Information

Dates: 07.03.2022 -- 11.03.2022

Time: 10:00 - 17:00

Examination: Research Protocol

Deadline for Protocol: 10.04.2021 (midnight)

## Code Notebooks

At the end of each day, we will upload notebooks with the code.

## Protocol

In your protocol you will follow the data analysis walkthrough from the course, but with a different dataset that we will make available here at the end of the course.

The code notebooks also contain questions that you need to answer in your protocols.

## Data

https://www.dropbox.com/s/gk5fxyl8vfpanji/E13A_full.zip

### Block 3 data:
Seurat object after block 3: [data/srt_03_e13a.rds](data/srt_03_e13a.rds)

Marker gene table: -- will be added here --

### Further resources
Mouse gene GO terms (ENSEMBL biomart output): -- will be added here --

## References

Carter RA, Bihannic L, Rosencrance C, Hadley JL, Tong Y, Phoenix TN, Natarajan S, Easton J, Northcott PA, Gawad C. A Single-Cell Transcriptional Atlas of the Developing Murine Cerebellum. Curr Biol. 2018 Sep 24;28(18):2910-2920.e2. doi: 10.1016/j.cub.2018.07.062. Epub 2018 Sep 13. PMID: 30220501.
